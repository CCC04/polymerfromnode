//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

//Agregamos componentes web bower
app.use(express.static(__dirname + '/build/ccc_v1'))

app.listen(port);

console.log('Executing Polymer from Node started on: ' + port);

app.get ("/",function(req, res) {
  //res.send('Hello World, Im An Api-CCC');
  //res.sendFile(path.join(__dirname,'index.html'));
  res.sendFile("index.html", {root:'.'});
})
/*
app.get ("/clientes/:idcliente",function(req, res) {
  res.send('GET,Cliente Recibido:' + req.params.idcliente );
  //res.sendFile(path.join(__dirname,'index.html'));
})
app.post ("/",function(req, res) {
  res.send('Peticion POST, Recibida modificada en Api-CCC');
})
app.delete ("/",function(req, res) {
  res.send('Peticion DELETE, Recibida en Api-CCC');
})
app.put ("/",function(req, res) {
  res.send('Peticion PUT, Recibida en Api-CCC');
})
*/

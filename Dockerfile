# Docker File of CCC
# Image Base
FROM node:latest

# DirLocation of app
WORKDIR /app

# Copy Files
ADD build/ccc_v1 /app/build/ccc_v1
ADD server.js /app
ADD package.json /app

#Dependencies
RUN npm install

#Port to Publish
EXPOSE 3001

#Command RUN
CMD ["npm", "start"]
